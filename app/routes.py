from app import app
from flask import request
import json


@app.route('/')
@app.route('/index', methods=['GET'])
def index():
    return "Hello, Mate!"


@app.route('/step_post_method', methods=['POST'])
def step_post_method():
    request_data = request.get_json()
    app.logger.info('Called step_post_method')
    app.logger.info(request_data)
    return json.dumps({'status': 'ok'}), 200


@app.route('/step_get_method', methods=['GET'])
@app.route('/step_get_method/<operation_id>',
           defaults={'operation_id': None}, methods=['GET'])
@app.route('/step_get_method/<operation_id>', methods=['GET'])
def step_get_method(operation_id):
    app.logger.info('Called step_get_method')
    app.logger.info(json.dumps({'operation_id': operation_id}))
    return json.dumps({'status': 'ok'}), 200


@app.route('/step_post_method_pending', methods=['POST'])
def step_post_method_pending():
    request_data = request.get_json()
    app.logger.info('Called step_post_method_pending')
    app.logger.info(request_data)
    return json.dumps({'status': 'ok'}), 202
