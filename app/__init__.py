from flask import Flask

app = Flask(__name__)

from app import routes

import logging

from logging import Formatter, FileHandler


file_handler = FileHandler('app.log')
file_handler.setLevel(logging.DEBUG)
app.logger.addHandler(file_handler)
